//
//  UIMacros.h
//  Muddleme
//
//  Created by Tahir Iqbal on 08/06/2015.
//  Copyright (c) 2015 AESquares. All rights reserved.
//

#ifndef UIMuddlemeMacros_h
#define UIMuddlemeMacros_h

static NSString *const KPlaceholderImage = @"brand-placeholder";

//Font Names
static NSString *const kFontRegularName = @"Helvetica";
static NSString *const kFontBoldName = @"Helvetica-Bold";
static NSString *const kFontLightName = @"Helvetica-Light";

static NSString *const kFontBaronNeueRegularName = @"BaronNeue";


#pragma mark - Fonts

#define FontRegular(pointSize) [UIFont fontWithName:kFontRegularName size:pointSize]
#define FontBold(pointSize) [UIFont fontWithName:kFontBoldName size:pointSize]
#define FontLight(pointSize) [UIFont fontWithName:kFontLightName size:pointSize]

#define FontBaronNeueRegular(pointSize) [UIFont fontWithName:kFontBaronNeueRegularName size:pointSize]


#pragma mark - String Building

#define PriceWithDefaultCurrency(price) [NSString stringWithFormat:@"%@ %0.2f", kBaseCurrency, price]
#define PriceStringWithCurrency(price, currency) [NSString stringWithFormat:@"%@ %0.2f", currency, price]

#pragma mark - PlaceHolders Text

#define PlaceHolderAttributedString(placeHolderText) [[NSAttributedString alloc] initWithString:placeHolderText attributes:@{NSForegroundColorAttributeName:ColorWhite}]


#pragma mark - UIImages

#define kNewBrandImages @[@"Shops_Bike", @"Shops_Book", @"Shops_Buchers", @"Shops_Cake", @"Shops_Camera", @"Shops_Cocktail", @"Shops_Coffee", @"Shops_Fruit", @"Shops_Internet", @"Shops_Jewel", @"Shops_Pasta", @"Shops_Shoe"]

// UIImage from Image Name
#define UIImageFromName(imageName) [UIImage imageNamed:imageName]


// Placeholder image
#define PlaceHolderImage [UIImage imageNamed:KPlaceholderImage]


#pragma mark - Colors

// NavBarBg Color
//#define ColorNavBarBg [UIColor colorWithRed:225./255. green:108./255. blue:70./255. alpha:1.]
#define ColorNavBarBg [UIColor colorWithRed:217./255. green:93./255. blue:39./255. alpha:1.]

// Light Background Color
#define ColorLightBg [UIColor colorWithRed:247./255. green:247./255. blue:247./255. alpha:1.]

// White Color
#define ColorWhite [UIColor whiteColor]

// Black Color
#define ColorBlack [UIColor blackColor]

// Purple Color
#define ColorPurple [UIColor colorWithRed:236./255. green:0./255. blue:147./255. alpha:1.]

// Dark Text Color
#define ColorDarkText [UIColor colorWithRed:98./255. green:98./255. blue:98./255. alpha:1.]

// Dark Background Color
#define ColorDarkBackground [UIColor colorWithRed:20./255. green:20./255. blue:20./255. alpha:1.]

// Section Header Bg Color
#define ColorSectionHeaderBG [UIColor colorWithRed:118.0/255.0 green:118.0/255.0 blue:118.0/255.0 alpha:1.0]

// Sunny Color
#define ColorSunny [UIColor colorWithRed:246./255. green:122./255. blue:22./255. alpha:1.]

// Contrast Color
#define ColorContrast [UIColor colorWithRed:35./255. green:35./255. blue:35./255. alpha:1.]

// Ocean Color
#define ColorOcean [UIColor colorWithRed:8./255. green:34./255. blue:61./255. alpha:1.]

// Pop Color
#define ColorPop [UIColor colorWithRed:208./255. green:0 blue:2./255. alpha:1.]

// Red Blue Color
#define ColorRedBlue [UIColor colorWithRed:37./255. green:183./255. blue:238./255. alpha:1.]

// Pink Lady Color
#define ColorPinkLady [UIColor colorWithRed:166./255. green:0 blue:72./255. alpha:1.]

// Red Color
#define ColorRed [UIColor redColor]

// Green Color
#define ColorGreen [UIColor greenColor]

// Clear Color
#define ColorClear [UIColor clearColor]

// Light Gray Color
#define ColorLightGray [UIColor lightGrayColor]


#endif
