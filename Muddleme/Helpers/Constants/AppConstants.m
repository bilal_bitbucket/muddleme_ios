//
//  AppConstants.m
//  Muddleme
//
//  Created by Tahir Iqbal on 08/06/2015.
//  Copyright (c) 2015 AESquares. All rights reserved.
//

#import "AppConstants.h"

@implementation AppConstants


#pragma mark - API Related

NSString *const kAPIBaseURL = @"http://muddleme.herokuapp.com/api/v1/";
NSString *const kAPIForgotPasswordPath = @"users/forget_password";
NSString *const kAPISignupPath = @"users/signup";
NSString *const kAPISigninPath = @"users/signin";
NSString *const kAPIUpdateProfilePath = @"users/update";
NSString *const kAPIChangePasswordPath = @"users/change_password";
NSString *const kAPIGetCurrenciesPath = @"companies/currencies";
NSString *const kAPISaveSuggestiveBrandsPath = @"brands/suggested_save";
NSString *const kAPIVoteSuggestiveBrandPath = @"brands/voting";
NSString *const kAPISaveUserLocationPath = @"user_locations/save";
NSString *const kAPIShippmentOptionPath = @"carts/shippings";
NSString *const kAPISaveUserCampaignsPath = @"campaigns/save";
NSString *const kAPICreatePaymentCard = @"credit_cards/create";
NSString *const kAPIMakePaymentCardDefault = @"credit_cards/update";
NSString *const kAPIDestroyPaymentCard = @"credit_cards/destroy";
NSString *const kAPIMethodPost = @"POST";
NSString *const kAPIMethodGet = @"GET";
NSString *const kAPIMethodPut = @"PUT";
NSString *const kAppStoreID = @"978228858";
NSString *const kEmptyString = @"";
NSString *const kDummyPasswordMask = @"xxxxxxxx";
NSString *const kComingSoonText = @"Coming Soon";



#pragma mark - Validation Messages

NSString *const kValidationMessageMissingInput = @"Please fill all the fields";
NSString *const kValidationMessageInvalidInput = @"Please enter valid information";



#pragma mark - NSDates

NSString *const kDateFormatStandard = @"yyyy-MM-dd HH:mm:ss.SZ";
NSString *const kDateFormatShort = @"yyyy-MM-dd";
NSString *const kDateFormatPaymentCardExpiry = @"MM/yy";


#pragma mark - UI

double const kAnimationDuration = 0.25;



#pragma mark - Pagination

NSInteger const kPageSizeForHomePage = 20;


NSString *const kUserBrandsCountKey = @"UserBrandsCountKey";
NSInteger const kAddNewBrandsLimit = 10;


#pragma mark - String Building

NSString *const kPointsFullString = @"Point";
NSString *const kStampsFullString = @"Stamp";
NSString *const kCashBackFullString = @"Discount";
NSString *const kPointsShortString = @"PTS";
NSString *const kStampsShortString = @"STM";
NSString *const kCashBackShortString = @"CBD";
NSString *const kPaymentOptionCashOnly = @"cash";
NSString *const kPaymentOptionCardOnly = @"cards";
NSString *const kPaymentOptionBoth = @"cash_and_cards";
NSString *const kBaseCurrency = @"GBP";
NSString *const kSharedManagerDumpFileName = @"sharedManager.dmp";



#pragma mark - UIImages

NSString *const kNavbarLogoImageName = @"logo_navbar";
NSString *const kRightNavigationButtonFilterCategories = @"categories-callout-filter";
NSString *const kRightNavigationButtonFilteredCategories = @"categories-callout-filtered";
NSString *const kRightNavigationButtonLogout = @"logout-button";
NSString *const kRightNavigationButtonAddNew = @"nav-add-button";
NSString *const kRightNavigationButtonMenu = @"nav_menu";
NSString *const kRightNavigationButtonMenuSelected = @"btn_navbar_close";
NSString *const kNavBackButton = @"btn_back";
NSString *const kNavBackButtonBlue = @"back-blue";


#pragma mark - Validations

NSInteger const kPasswordRequiredLength = 6;
NSInteger const kUserNameRequiredLength = 4;
NSInteger const kPhoneNumberRequiredLength = 4;
NSInteger const kAddressRequiredLength = 4;
NSInteger const kPaymentCardNumberRequiredLength = 15;
NSInteger const kPaymentCardExpiryLength = 5;
NSInteger const kPaymentCardCSVRequiredLength = 3;
NSInteger const kCityRequiredLength = 2;
NSInteger const kZipCodeRequiredLength = 4;
NSInteger const kAreaCodeRequiredLength = 3;
NSInteger const kStateRequiredLength = 2;
NSInteger const kCountryRequiredLength = 2;
NSInteger const kAppPINRequiredLength = 4;
NSInteger const kPersonalizationFormFieldRequiredLength = 1;

#pragma mark - AlertView

NSString *const kLogoutMessage = @"Are you sure you want to logout?";
NSString *const kPasswordChangeAlert = @"Please enter your current password in order to change it to new one";
NSString *const kApplyRewardAlert = @"You can't perform this action on this product";
NSString *const kEmptyBasketAlert = @"Your basket is empty";
NSString *const kBasketProductCannotBurnMessage = @"This item cannot be paid by the Loyalty Card.";
NSString *const kBasketCollectAtStore = @"Collect at store";
NSString *const kBasketPaymentInPerson = @"Payment in person";
NSString *const kPaymentCardAlreadyExist = @"Card already exists.";

NSString *const kNewBrandText = @"We need more votes to get this brand open a branch on your Muddleme";


#pragma mark - SearchfieldFont

NSString *const kSearchFieldFontName = @"HelveticaNeue";

#pragma  mark - BrainTree Constant

NSString *const kBrainTreeDemoToken = @"eyJ2ZXJzaW9uIjoyLCJhdXRob3JpemF0aW9uRmluZ2VycHJpbnQiOiJjODA0Zjg0NDg3MDllMGQxNDRiNjg3NmQ2NDk3ZjgyM2ZiY2QwNzJmOTBhMzhiYzhhMzg0NjQwZjRmMTVmYjg3fGNyZWF0ZWRfYXQ9MjAxNS0wNi0xOVQwNzo0MzowMi45Nzg4MzUwMTcrMDAwMFx1MDAyNm1lcmNoYW50X2lkPWRjcHNweTJicndkanIzcW5cdTAwMjZwdWJsaWNfa2V5PTl3d3J6cWszdnIzdDRuYzgiLCJjb25maWdVcmwiOiJodHRwczovL2FwaS5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tOjQ0My9tZXJjaGFudHMvZGNwc3B5MmJyd2RqcjNxbi9jbGllbnRfYXBpL3YxL2NvbmZpZ3VyYXRpb24iLCJjaGFsbGVuZ2VzIjpbXSwiZW52aXJvbm1lbnQiOiJzYW5kYm94IiwiY2xpZW50QXBpVXJsIjoiaHR0cHM6Ly9hcGkuc2FuZGJveC5icmFpbnRyZWVnYXRld2F5LmNvbTo0NDMvbWVyY2hhbnRzL2RjcHNweTJicndkanIzcW4vY2xpZW50X2FwaSIsImFzc2V0c1VybCI6Imh0dHBzOi8vYXNzZXRzLmJyYWludHJlZWdhdGV3YXkuY29tIiwiYXV0aFVybCI6Imh0dHBzOi8vYXV0aC52ZW5tby5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tIiwiYW5hbHl0aWNzIjp7InVybCI6Imh0dHBzOi8vY2xpZW50LWFuYWx5dGljcy5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tIn0sInRocmVlRFNlY3VyZUVuYWJsZWQiOnRydWUsInRocmVlRFNlY3VyZSI6eyJsb29rdXBVcmwiOiJodHRwczovL2FwaS5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tOjQ0My9tZXJjaGFudHMvZGNwc3B5MmJyd2RqcjNxbi90aHJlZV9kX3NlY3VyZS9sb29rdXAifSwicGF5cGFsRW5hYmxlZCI6dHJ1ZSwicGF5cGFsIjp7ImRpc3BsYXlOYW1lIjoiQWNtZSBXaWRnZXRzLCBMdGQuIChTYW5kYm94KSIsImNsaWVudElkIjpudWxsLCJwcml2YWN5VXJsIjoiaHR0cDovL2V4YW1wbGUuY29tL3BwIiwidXNlckFncmVlbWVudFVybCI6Imh0dHA6Ly9leGFtcGxlLmNvbS90b3MiLCJiYXNlVXJsIjoiaHR0cHM6Ly9hc3NldHMuYnJhaW50cmVlZ2F0ZXdheS5jb20iLCJhc3NldHNVcmwiOiJodHRwczovL2NoZWNrb3V0LnBheXBhbC5jb20iLCJkaXJlY3RCYXNlVXJsIjpudWxsLCJhbGxvd0h0dHAiOnRydWUsImVudmlyb25tZW50Tm9OZXR3b3JrIjp0cnVlLCJlbnZpcm9ubWVudCI6Im9mZmxpbmUiLCJ1bnZldHRlZE1lcmNoYW50IjpmYWxzZSwiYnJhaW50cmVlQ2xpZW50SWQiOiJtYXN0ZXJjbGllbnQzIiwibWVyY2hhbnRBY2NvdW50SWQiOiJzdGNoMm5mZGZ3c3p5dHc1IiwiY3VycmVuY3lJc29Db2RlIjoiVVNEIn0sImNvaW5iYXNlRW5hYmxlZCI6dHJ1ZSwiY29pbmJhc2UiOnsiY2xpZW50SWQiOiIxMWQyNzIyOWJhNThiNTZkN2UzYzAxYTA1MjdmNGQ1YjQ0NmQ0ZjY4NDgxN2NiNjIzZDI1NWI1NzNhZGRjNTliIiwibWVyY2hhbnRBY2NvdW50IjoiY29pbmJhc2UtZGV2ZWxvcG1lbnQtbWVyY2hhbnRAZ2V0YnJhaW50cmVlLmNvbSIsInNjb3BlcyI6ImF1dGhvcml6YXRpb25zOmJyYWludHJlZSB1c2VyIiwicmVkaXJlY3RVcmwiOiJodHRwczovL2Fzc2V0cy5icmFpbnRyZWVnYXRld2F5LmNvbS9jb2luYmFzZS9vYXV0aC9yZWRpcmVjdC1sYW5kaW5nLmh0bWwiLCJlbnZpcm9ubWVudCI6Im1vY2sifSwibWVyY2hhbnRJZCI6ImRjcHNweTJicndkanIzcW4iLCJ2ZW5tbyI6Im9mZmxpbmUiLCJhcHBsZVBheSI6eyJzdGF0dXMiOiJtb2NrIiwiY291bnRyeUNvZGUiOiJVUyIsImN1cnJlbmN5Q29kZSI6IlVTRCIsIm1lcmNoYW50SWRlbnRpZmllciI6Im1lcmNoYW50LmNvbS5icmFpbnRyZWVwYXltZW50cy5zYW5kYm94LkJyYWludHJlZS1EZW1vIiwic3VwcG9ydGVkTmV0d29ya3MiOlsidmlzYSIsIm1hc3RlcmNhcmQiLCJhbWV4Il19fQ==";



@end
