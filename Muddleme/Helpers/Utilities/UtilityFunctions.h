//
//  UtilityFunctions.h
//  Muddleme
//
//  Created by Asad Ali on 24/03/2015.
//  Copyright (c) 2015 AESquares. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface UtilityFunctions : NSObject


#pragma mark - Validation Methods

+ (BOOL)isValidEmailAddress:(NSString *)emailText;

+ (void)setupApplicationUIAppearance;


+ (void)setValueInUserDefaults:(id)value forKey:(NSString *)key;

+ (id)getValueFromUserDefaultsForKey:(NSString *)key;


+ (NSAttributedString *)attributedStringForBasketCallout:(NSString *)string;


+ (NSAttributedString *)attributedStringForBasketCalloutWithCurrency:(float)price;


+ (NSAttributedString *)attributedStringForBasketForPrice:(float)price currency:(NSString *)currency;


+ (NSAttributedString *)attributedStringWithPrice:(float)price forCurrency:(NSString *)currency priceFont:(UIFont *)priceFont currencyFont:(UIFont *)currencyFont priceColor:(UIColor *)priceColor currencyColor:(UIColor *)currencyColor;


@end
