//
//  UtilityFunctions.m
//  Muddleme
//
//  Created by Asad Ali on 24/03/2015.
//  Copyright (c) 2015 AESquares. All rights reserved.
//

#import "UtilityFunctions.h"
#import <UIKit/UIKit.h>
#import "AppConstants.h"
#import "UIMacros.h"

@implementation UtilityFunctions


#pragma mark - Validation Methods

+ (BOOL)isValidEmailAddress:(NSString *)emailText
{
    
    NSString *emailRegEx =
    @"(?:[a-zA-Z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-zA-Z0-9!#$%\\&'*+/=?\\^_`{|}"
    @"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
    @"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-zA-Z0-9](?:[a-"
    @"zA-Z0-9-]*[a-zA-Z0-9])?\\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?|\\[(?:(?:25[0-5"
    @"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
    @"9][0-9]?|[a-zA-Z0-9-]*[a-zA-Z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
    @"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    
    NSPredicate *regExPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    BOOL isValid = [regExPredicate evaluateWithObject:emailText];
    
    return isValid;
}


+ (void)setupApplicationUIAppearance
{
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:ColorWhite, NSFontAttributeName:FontBaronNeueRegular(14.0)}];
    
    [[UINavigationBar appearance] setBarTintColor:ColorNavBarBg];

//    [[UINavigationBar appearance] setBackgroundImage:UIImageFromName(@"navbar_bg") forBarMetrics:UIBarMetricsDefault];
    
    [[UIBarButtonItem appearance] setBackButtonBackgroundImage:[UIImage alloc] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    
    [[UINavigationBar appearance] setBackIndicatorImage:[UIImage alloc]];
    [[UINavigationBar appearance] setBackIndicatorTransitionMaskImage:[UIImage alloc]];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    [[UINavigationBar appearance] setShadowImage:[UIImage alloc]];
    
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setFont:[UIFont fontWithName:kSearchFieldFontName size:13]];
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setTextColor:[UIColor colorWithRed:61./255. green:61./255. blue:61./255. alpha:1.]];
    
    [[UILabel appearanceWhenContainedIn:[UITableViewHeaderFooterView class], nil] setFont:FontRegular(14.0)];
    
    
    [[UISearchBar appearance] setBarTintColor:ColorNavBarBg];

    [[UIBarButtonItem appearanceWhenContainedIn:[UISearchBar class], nil] setTitleTextAttributes:@{NSForegroundColorAttributeName:ColorDarkText}
                                                                                        forState:UIControlStateNormal];
}


+ (void)setValueInUserDefaults:(id)value forKey:(NSString *)key
{
    [[NSUserDefaults standardUserDefaults] setValue:value forKey:key];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
}


+ (id)getValueFromUserDefaultsForKey:(NSString *)key
{
    return [[NSUserDefaults standardUserDefaults] valueForKey:key];
}


+ (NSAttributedString *)attributedStringForBasketCallout:(NSString *)string
{
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:string];
    
    
    [attributedString addAttributes:@{NSFontAttributeName:FontRegular(13.0), NSForegroundColorAttributeName:ColorDarkText} range:NSMakeRange(0, string.length)];
    
    
    return attributedString;
}


+ (NSAttributedString *)attributedStringForBasketCalloutWithCurrency:(float)price
{
    /*
     [[NSAttributedString alloc] initWithString:@"" attributes:@{NSStrikethroughStyleAttributeName:@(NSUnderlineStyleSingle)}];
     */
    return [UtilityFunctions attributedStringWithPrice:price forCurrency:kBaseCurrency priceFont:FontRegular(12.0) currencyFont:FontLight(8.0) priceColor:ColorDarkText currencyColor:ColorLightGray];
}

+ (NSAttributedString *)attributedStringForBasketForPrice:(float)price currency:(NSString *)currency
{
    return [UtilityFunctions attributedStringWithPrice:price forCurrency:currency priceFont:FontLight(15.0) currencyFont:FontLight(8.0) priceColor:ColorDarkText currencyColor:ColorLightGray];
}


+ (NSAttributedString *)attributedStringWithPrice:(float)price forCurrency:(NSString *)currency priceFont:(UIFont *)priceFont currencyFont:(UIFont *)currencyFont priceColor:(UIColor *)priceColor currencyColor:(UIColor *)currencyColor
{
    NSString *priceString = [NSString stringWithFormat:@"%0.2f", price];
    NSString *completeString = [NSString stringWithFormat:@"%@ %@", priceString, currency];
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:completeString];
    
    
    [attributedString addAttributes:@{NSFontAttributeName:currencyFont, NSForegroundColorAttributeName:currencyColor} range:[completeString rangeOfString:currency]];
    [attributedString addAttributes:@{NSFontAttributeName:priceFont, NSForegroundColorAttributeName:priceColor} range:[completeString rangeOfString:priceString]];
    
    
    return attributedString;
}


@end
