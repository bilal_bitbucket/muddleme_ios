//
//  SizeClass.h
//  Tellum
//
//  Created by Asad Ali on 08/04/2015.
//  Copyright (c) 2015 AESquares. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SizeClass : NSObject



@property (nonatomic, readonly) BOOL isDeviceIPad;
@property (nonatomic, readonly) BOOL isDeviceIPhone4;
@property (nonatomic, readonly) BOOL isDeviceIPhone5;
@property (nonatomic, readonly) BOOL isDeviceIPhone6;
@property (nonatomic, readonly) BOOL isDeviceIPhone6Plus;


@property (nonatomic, readonly) float deviceWidth;
@property (nonatomic, readonly) float deviceHeight;

@property (nonatomic, readonly) float onePixelHeight;

@property (nonatomic, readonly) float tabbarHeight;
@property (nonatomic, readonly) float collectionViewWidth;
@property (nonatomic, readonly) float collectionViewCellSpacing;
@property (nonatomic, readonly) float collectionViewLineSpacing;


@property (nonatomic, readonly) float collectionViewCellWidth;


@property (nonatomic, readonly) float cellImageViewWidth;
@property (nonatomic, readonly) float cellImageViewPadding;
@property (nonatomic, readonly) float cellImageViewWithImageHeight;
@property (nonatomic, readonly) float cellImageViewWithoutImageHeight;
@property (nonatomic, readonly) float cellLogoImageWidth;


@property (nonatomic, readonly) float cellLabelsStandardPadding;


@property (nonatomic, readonly) float TellumCellLabelsPadding;
@property (nonatomic, readonly) float TellumCellTitleLabelHeight;
@property (nonatomic, readonly) float TellumCellDescriptionLabelWidth;
@property (nonatomic, readonly) float TellumCellDescriptionLabelDefaultTopConstant;

@property (nonatomic, readonly) float TellumNewBrandCellHeight;

@property (nonatomic, readonly) float TellumCellBottomActionButtonHeight;


@property (nonatomic, readonly) float autopilotCellTitleLabelHeight;
@property (nonatomic, readonly) float autopilotCellTitleLabelPadding;
@property (nonatomic, readonly) float autopilotCellOptionViewsPadding;
@property (nonatomic, readonly) float autopilotCellOptionViewsHeight;



@property (nonatomic, readonly) float productCellTitleLabelLeftPadding;
@property (nonatomic, readonly) float productCellLoyalityCollectionViewHeight;
@property (nonatomic, readonly) float productCellCampaignCollectionViewHeight;
@property (nonatomic, readonly) float productCellPagerViewHeight;
@property (nonatomic, readonly) float productCellSelectedPropertiesViewHeight;
@property (nonatomic, readonly) float productCellPriceLabelHeight;
@property (nonatomic, readonly) float productCellQuantityViewHeight;
@property (nonatomic, readonly) float productPropertyCellTitleFontSize;


@property (nonatomic, readonly) float personalizationFormPopupTopSpace;
@property (nonatomic, readonly) float personalizationFormPopupHeaderHeight;
@property (nonatomic, readonly) float personalizationFormPopupStandardPadding;
@property (nonatomic, readonly) float personalizationFormPopupTableCellHeight;
@property (nonatomic, readonly) float personalizationFormPopupSaveButtonHeight;
@property (nonatomic, readonly) float personalizationFormPopupBottomPadding;



@property (nonatomic, readonly) float storeCellAddressLabelTopPadding;
@property (nonatomic, readonly) float storeCellTitleLabelHeight;



@property (nonatomic, readonly) float meCardsOptionViewHeight;



@property (nonatomic, readonly) float meAccountProfileImageHeight;


@property (nonatomic, readonly) float homeSearchViewHeight;
@property (nonatomic, readonly) float homePopupViewHeight;
@property (nonatomic, readonly) float homePopupViewWidth;


@property (nonatomic, readonly) float meLocationAddNewLocationViewHeight;
@property (nonatomic, readonly) float meLocationCellHeight;

@property (nonatomic, readonly) float meLoyaltyCardCellHeight;
@property (nonatomic, readonly) float meLoyaltyCardCellWithoutTextHeight;
@property (nonatomic, readonly) float meLoyaltyCardLogoHeight;

@property (nonatomic, readonly) float mePaymentCardAddNewViewHeight;
@property (nonatomic, readonly) float mePaymentCellHeight;


@property (nonatomic, readonly) float basketLogoHeight;
@property (nonatomic, readonly) float basketLogoViewHeight;
@property (nonatomic, readonly) float basketPaymentViewHeight;
@property (nonatomic, readonly) float basketInvoiceTitleViewHeight;
@property (nonatomic, readonly) float basketDeliveryChargesViewHeight;
@property (nonatomic, readonly) float basketGrandTotalViewHeight;
@property (nonatomic, readonly) float basketCitiCardViewHeight;
@property (nonatomic, readonly) float basketTableViewCellHeight;
@property (nonatomic, readonly) float basketTableViewCellWithPointsHeight;




+ (SizeClass *)currentDeviceSizes;




@end
