//
//  UITableViewCell+Helper.h
//  Muddleme
//
//  Created by Asad Ali on 26/05/2015.
//  Copyright (c) 2015 AESquares. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableViewCell (Helper)


- (void)removeCellSeparatorOffset;


@end
