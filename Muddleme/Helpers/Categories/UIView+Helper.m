//
//  UIView+Helper.m
//  Muddleme
//
//  Created by Asad Ali on 24/03/2015.
//  Copyright (c) 2015 AESquares. All rights reserved.
//

#import "UIView+Helper.h"
//#import "UIConstants.h
#import "UIMacros.h"


#define DEGREES_TO_RADIANS(degrees)  ((3.14159265359 * degrees)/ 180)


@implementation UIView (Helper)


- (CGFloat)width
{
    return self.frame.size.width;
}

- (CGFloat)height
{
    return self.frame.size.height;
}

- (CGFloat)xPosition
{
    return self.frame.origin.x;
}

- (CGFloat)yPosition
{
    return self.frame.origin.y;
}


- (void)addUpperBorder
{
    CALayer *upperBorder = [CALayer layer];
    
    upperBorder.backgroundColor = [[UIColor grayColor] CGColor];
    upperBorder.frame = CGRectMake(0, 0, CGRectGetWidth(self.frame), 0.5f);
    
    [self.layer addSublayer:upperBorder];
}


- (void)addBorderWithWidth:(CGFloat)borderWidth borderColor:(UIColor *)borderColor
{
    self.layer.borderWidth = borderWidth;
    
    self.layer.borderColor = borderColor.CGColor;
}

- (void)removeBorder
{
    [self addBorderWithWidth:0.0 borderColor:ColorClear];
}


- (void)addShadow:(CGFloat)radius
{
    self.layer.masksToBounds = NO;
    self.layer.shadowColor = ColorDarkText.CGColor;
    self.layer.shadowOpacity = 0.15;
    self.layer.shadowOffset = CGSizeMake(0, 0);
    self.layer.shadowRadius = radius;
}


- (void)roundTheCorners:(CGFloat)radius
{
    self.layer.masksToBounds = YES;
    self.layer.cornerRadius = radius;
}


- (void)roundTheCornersWithBorder:(CGFloat)radius
{
    [self roundTheCorners:radius];
    
    
    [self addBorderWithWidth:0.5 borderColor:ColorDarkText];
}

- (void)roundedView
{
    [self roundTheCorners:self.bounds.size.width/2];    
}

- (void)roundedViewWithBorderWidth:(CGFloat)width borderColor:(UIColor *)color
{
    [self roundTheCorners:self.bounds.size.width/2];
    
    
    [self addBorderWithWidth:width borderColor:color];
}


//[self setRoundedViewCorners:(UIRectCornerBottomRight|UIRectCornerBottomLeft) view:self.tabbarView radius:7.0];
- (void)setRoundedCorners:(UIRectCorner)corners radius:(float)radius// viewSize:(CGSize)size
{
    CAShapeLayer * maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: self.bounds byRoundingCorners:corners cornerRadii: (CGSize){radius, radius}].CGPath;
    
    self.layer.mask = maskLayer;
    /*UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds byRoundingCorners:corners cornerRadii:CGSizeMake(radius, radius)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.bounds;//CGRectMake(0, 0, size.width, size.height);
    maskLayer.path = maskPath.CGPath;
    self.layer.mask = maskLayer;*/
    
//    CAShapeLayer *strokeLayer = [CAShapeLayer layer];
//    strokeLayer.path = maskPath.CGPath;
//    strokeLayer.fillColor = [UIColor clearColor].CGColor;
//    strokeLayer.strokeColor = [UIColor lightGrayColor].CGColor;
//    strokeLayer.lineWidth = 1;
    
//    UIView *strokeView = [[UIView alloc] initWithFrame:self.bounds];
//    strokeView.userInteractionEnabled = NO;
//    [strokeView.layer addSublayer:strokeLayer];
    
//    [self addSubview:strokeView];
}

- (void)createHalfCircleView:(UIView *)view
{
    UIBezierPath *path = [UIBezierPath bezierPathWithArcCenter:CGPointMake(view.frame.size.width/2, view.frame.size.width/2) radius:view.frame.size.width/2 startAngle:DEGREES_TO_RADIANS(180) endAngle:0 clockwise:YES];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = view.bounds;
    maskLayer.path = path.CGPath;
    view.layer.mask = maskLayer;
}



- (void)removeAllSubviews
{
    for (UIView *subView in self.subviews)
    {
        [subView removeFromSuperview];
    }
}


@end
