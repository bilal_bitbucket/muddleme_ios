//
//  NSString+Helper.h
//  Muddleme
//
//  Created by Asad Ali on 25/03/2015.
//  Copyright (c) 2015 AESquares. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NSString (Helper)


@property (readonly, nonatomic) NSString *standardDateString;

@property (readonly, nonatomic) NSString *cleanedString;



- (NSURL *)URL;

- (CGSize)textSizeWithFont:(UIFont *)font;

- (CGSize)textSizeForWidth:(CGFloat)width withFont:(UIFont *)font;

- (CGSize)sizeForHomeCellDescriptionTextForWidth:(CGFloat)width;

- (id)ParseJSONString;

- (NSAttributedString *)underlineAttributedStringForFont:(UIFont *)font textColor:(UIColor *)color;

- (NSAttributedString *)stringWithIncreasedLineSpacingForFont:(UIFont *)font textColor:(UIColor *)color;

- (NSAttributedString *)descriptionTextStringWithIncreasedLineSpacing;

- (NSString *)stringByRemovingCurrencyString;

- (NSString *)stringByRemovingFirstAndLastCharacter;

- (NSString *)removeLeadingAndTrailingSpaces;

- (NSString *)maskedCardNumberString;


@end




@interface NSAttributedString (Helper)


- (NSAttributedString *)underlineAttributedString;

- (CGSize)textSizeForWidth:(CGFloat)width;



@end