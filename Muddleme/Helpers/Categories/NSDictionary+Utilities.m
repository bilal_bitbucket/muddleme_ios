//
//  NSDictionary+Utilities.m
//  ConsumerBreak
//
//  Created by My Mac on 17/11/2014.
//  Copyright (c) 2014 Zeeshan Haider. All rights reserved.
//

#import "NSDictionary+Utilities.h"
#import <math.h>


@implementation NSDictionary (Utilities)




#pragma mark - Muddleme Data Helper


- (BOOL)isProductHasCampaigns
{
    NSLog(@"Campaign: %@", [self objectForKeyNotNull:@"campaigns"]);
    
    return [[self objectForKeyNotNull:@"campaigns"] count] > 0;
}


- (BOOL)isProductInAppPurchasable
{
    NSLog(@"Mode: %@", [self objectForKeyNotNull:@"product_deliver"]);
    
    return ![[self objectForKeyNotNull:@"product_deliver"] isEqualToString:@"shop"];
}


- (BOOL)isDictionaryContainsBrandData
{
    return [self containsKey:@"logo"];
}



- (NSString *)brandStoreAddress
{
    NSMutableString *addressString = [NSMutableString new];
    
    if ([self[@"address1"] length] > 0)
    {
        [addressString appendFormat:@"%@\n", self[@"address1"]];
    }
    if ([self[@"address2"] length] > 0)
    {
        [addressString appendFormat:@"%@\n", self[@"address2"]];
    }
    if ([self[@"address3"] length] > 0)
    {
        [addressString appendFormat:@"%@\n", self[@"address3"]];
    }
    
    if ([self[@"city"] length] > 0 && [self[@"state"] length] > 0 && [self[@"country"] length] > 0)
    {
        [addressString appendFormat:@"%@, %@, %@\n", self[@"city"], self[@"state"], self[@"country"]];
    }
    else
    {
        if ([self[@"city"] length] > 0)
        {
            [addressString appendFormat:@"%@", self[@"city"]];
        }
        if ([self[@"state"] length] > 0)
        {
            [addressString appendFormat:@", %@", self[@"state"]];
        }
        if ([self[@"country"] length] > 0)
        {
            [addressString appendFormat:@", %@", self[@"country"]];
        }
    }
    if ([self[@"phone"] length] > 0)
    {
        [addressString appendFormat:@"%@", self[@"phone"]];
    }
    
    
    return addressString;
}




#pragma mark - Utilities



// in case of [NSNull null] values a nil is returned ...
- (id)objectForKeyNotNull:(id)key
{
    id object = [self objectForKey:key];
    if (object == [NSNull null])
        return @"";
    
    return object;
}

// if objectForKey returns nil, it will
// return an empty string.
- (id)objectForKeyWithEmptyString:(id)key
{
    id object = [self objectForKeyNotNull:key];
    if (object == nil)
    {
        return @"";
    }
    return object;
}
- (BOOL)isKindOfClass:(Class)aClass forKey:(NSString *)key
{
    id value = [self objectForKey:key];
    return [value isKindOfClass:aClass];
}

- (BOOL)isMemberOfClass:(Class)aClass forKey:(NSString *)key
{
    id value = [self objectForKey:key];
    return [value isMemberOfClass:aClass];
}

- (BOOL)isArrayForKey:(NSString *)key
{
    return [self isKindOfClass:[NSArray class] forKey:key];
}

- (BOOL)isDictionaryForKey:(NSString *)key
{
    return [self isKindOfClass:[NSDictionary class] forKey:key];
}

- (BOOL)isStringForKey:(NSString *)key
{
    return [self isKindOfClass:[NSString class] forKey:key];
}

- (BOOL)isNumberForKey:(NSString *)key
{
    return [self isKindOfClass:[NSNumber class] forKey:key];
}

- (NSArray *)arrayForKey:(NSString *)key
{
    id value = [self objectForKey:key];
    if ([value isKindOfClass:[NSArray class]]) {
        return value;
    }
    return nil;
}

- (NSDictionary *)dictionaryForKey:(NSString *)key
{
    id value = [self objectForKey:key];
    if ([value isKindOfClass:[NSDictionary class]]) {
        return value;
    }
    return nil;
}

- (NSString *)stringForKey:(NSString *)key
{
    id value = [self objectForKey:key];
    if ([value isKindOfClass:[NSString class]]) {
        return value;
    } else if ([value respondsToSelector:@selector(description)]) {
        return [value description];
    }
    return nil;
}

- (NSNumber *)numberForKey:(NSString *)key
{
    id value = [self objectForKey:key];
    if ([value isKindOfClass:[NSNumber class]]) {
        return value;
    } else if ([value isKindOfClass:[NSString class]]) {
        NSNumberFormatter *nf = [[NSNumberFormatter alloc] init];
        return [nf numberFromString:value];
    }
    return nil;
}

- (double)doubleForKey:(NSString *)key
{
    id value = [self objectForKey:key];
    if ([value isKindOfClass:[NSString class]] || [value isKindOfClass:[NSNumber class]]) {
        return [value doubleValue];
    }
    return 0;
}

- (float)floatForKey:(NSString *)key
{
    id value = [self objectForKey:key];
    if ([value isKindOfClass:[NSString class]] || [value isKindOfClass:[NSNumber class]]) {
        return [value floatValue];
    }
    return 0;
}

- (int)intForKey:(NSString *)key
{
    id value = [self objectForKey:key];
    if ([value isKindOfClass:[NSString class]] || [value isKindOfClass:[NSNumber class]]) {
        return [value intValue];
    }
    return 0;
}

- (unsigned int)unsignedIntForKey:(NSString *)key
{
    id value = [self objectForKey:key];
    if ([value isKindOfClass:[NSString class]]) {
        NSNumberFormatter *nf = [[NSNumberFormatter alloc] init];
        value = [nf numberFromString:value];
    }
    if ([value isKindOfClass:[NSNumber class]]) {
        return [value unsignedIntValue];
    }
    return 0;
}

- (NSInteger)integerForKey:(NSString *)key
{
    id value = [self objectForKey:key];
    if ([value isKindOfClass:[NSString class]] || [value isKindOfClass:[NSNumber class]]) {
        return [value integerValue];
    }
    return 0;
}

- (NSUInteger)unsignedIntegerForKey:(NSString *)key
{
    id value = [self objectForKey:key];
    if ([value isKindOfClass:[NSString class]]) {
        NSNumberFormatter *nf = [[NSNumberFormatter alloc] init];
        value = [nf numberFromString:value];
    }
    if ([value isKindOfClass:[NSNumber class]]) {
        return [value unsignedIntegerValue];
    }
    return 0;
}

- (long long)longLongForKey:(NSString *)key
{
    id value = [self objectForKey:key];
    if ([value isKindOfClass:[NSString class]] || [value isKindOfClass:[NSNumber class]]) {
        return [value longLongValue];
    }
    return 0;
}

- (unsigned long long)unsignedLongLongForKey:(NSString *)key
{
    id value = [self objectForKey:key];
    if ([value isKindOfClass:[NSString class]]) {
        NSNumberFormatter *nf = [[NSNumberFormatter alloc] init];
        value = [nf numberFromString:value];
    }
    if ([value isKindOfClass:[NSNumber class]]) {
        return [value unsignedLongLongValue];
    }
    return 0;
}

- (BOOL)boolForKey:(NSString *)key
{
    id value = [self objectForKey:key];
    if ([value isKindOfClass:[NSString class]] || [value isKindOfClass:[NSNumber class]]) {
        return [value boolValue];
    }
    return NO;
}

- (bool)existsValue:(NSString*)expectedValue forKey:(NSString*)key
{
    id val = [self valueForKey:key];
    bool exists = false;
    
    if (val != nil) {
        exists = [(NSString*)val compare : expectedValue options : NSCaseInsensitiveSearch] == 0;
    }
    
    return exists;
}
- (BOOL)containsKey: (NSString *)key {
    BOOL retVal = 0;
    NSArray *allKeys = [self allKeys];
    retVal = [allKeys containsObject:key];
    return retVal;
}
- (NSInteger)integerValueForKey:(NSString*)key defaultValue:(NSInteger)defaultValue withRange:(NSRange)range
{
    NSInteger value = defaultValue;
    
    NSNumber* val = [self valueForKey:key];  // value is an NSNumber
    
    if (val != nil) {
        value = [val integerValue];
    }
    
    // min, max checks
    value = MAX(range.location, value);
    value = MIN(range.length, value);
    
    return value;
}

- (NSInteger)integerValueForKey:(NSString*)key defaultValue:(NSInteger)defaultValue
{
    NSInteger value = defaultValue;
    
    NSNumber* val = [self valueForKey:key];  // value is an NSNumber
    
    if (val != nil) {
        value = [val integerValue];
    }
    return value;
}

/*
 *	Determine the type of object stored in a dictionary
 *	IN:
 *	(BOOL*) bString - if exists will be set to YES if object is an NSString, NO if not
 *	(BOOL*) bNull - if exists will be set to YES if object is an NSNull, NO if not
 *	(BOOL*) bArray - if exists will be set to YES if object is an NSArray, NO if not
 *	(BOOL*) bNumber - if exists will be set to YES if object is an NSNumber, NO if not
 *
 *	OUT:
 *	YES if key exists
 *  NO if key does not exist.  Input parameters remain untouched
 *
 */

- (BOOL)typeValueForKey:(NSString*)key isArray:(BOOL*)bArray isNull:(BOOL*)bNull isNumber:(BOOL*)bNumber isString:(BOOL*)bString
{
    BOOL bExists = YES;
    NSObject* value = [self objectForKey:key];
    
    if (value) {
        bExists = YES;
        if (bString) {
            *bString = [value isKindOfClass:[NSString class]];
        }
        if (bNull) {
            *bNull = [value isKindOfClass:[NSNull class]];
        }
        if (bArray) {
            *bArray = [value isKindOfClass:[NSArray class]];
        }
        if (bNumber) {
            *bNumber = [value isKindOfClass:[NSNumber class]];
        }
    }
    return bExists;
}

- (BOOL)valueForKeyIsArray:(NSString*)key
{
    BOOL bArray = NO;
    NSObject* value = [self objectForKey:key];
    
    if (value) {
        bArray = [value isKindOfClass:[NSArray class]];
    }
    return bArray;
}

- (BOOL)valueForKeyIsNull:(NSString*)key
{
    BOOL bNull = NO;
    NSObject* value = [self objectForKey:key];
    
    if (value) {
        bNull = [value isKindOfClass:[NSNull class]];
    }
    return bNull;
}

- (BOOL)valueForKeyIsString:(NSString*)key
{
    BOOL bString = NO;
    NSObject* value = [self objectForKey:key];
    
    if (value) {
        bString = [value isKindOfClass:[NSString class]];
    }
    return bString;
}

- (BOOL)valueForKeyIsNumber:(NSString*)key
{
    BOOL bNumber = NO;
    NSObject* value = [self objectForKey:key];
    
    if (value) {
        bNumber = [value isKindOfClass:[NSNumber class]];
    }
    return bNumber;
}

- (NSDictionary*)dictionaryWithLowercaseKeys
{
    NSMutableDictionary* result = [NSMutableDictionary dictionaryWithCapacity:self.count];
    NSString* key;
    
    for (key in self) {
        [result setObject:[self objectForKey:key] forKey:[key lowercaseString]];
    }
    
    return result;
}
+(NSDictionary*)dictionaryWithContentsOfJSONString:(NSString*)fileLocation
{
    NSString *string = fileLocation;

    NSString *filePath = [[NSBundle bundleWithPath:string] pathForResource:@"Data" ofType:@"json"];
    NSData* data = [NSData dataWithContentsOfFile:filePath];
    __autoreleasing NSError* error = nil;
    id result = [NSJSONSerialization JSONObjectWithData:data
                                                options:kNilOptions error:&error];
    // Be careful here. You add this as a category to NSDictionary
    // but you get an id back, which means that result
    // might be an NSArray as well!
    if (error != nil) return nil;
    return result;
}
@end
