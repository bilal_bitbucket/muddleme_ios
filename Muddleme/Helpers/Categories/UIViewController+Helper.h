//
//  UIViewController+Helper.h
//  Muddleme
//
//  Created by Asad Ali on 25/03/2015.
//  Copyright (c) 2015 AESquares. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef enum {
    ActionSheetUserSelectionPhotoLibrary,
    ActionSheetUserSelectionCamera,
} ActionSheetUserSelection;


typedef void (^AlertViewDismissHandler)(void);
typedef void (^AlertViewButtonClickedAtIndexHandler)(ActionSheetUserSelection selectedOption);
typedef void (^AlertViewConfirmedButtonClickedHandler)(void);
typedef void (^AlertViewCurrentPasswordConfirmedHandler)(NSString *currentPassword);

@interface UIViewController (Helper)



- (void)addChildViewControllerInContainer:(UIView *)containerView childViewController:(UIViewController *)controller;

- (void)addChildViewControllerInContainer:(UIView *)containerView childViewController:(UIViewController *)controller preserverViewController:(UIViewController *)dontDeleteVC;

- (void)removeAllChildViewControllers;

- (void)removeAllChildViewControllersExcept:(UIViewController *)vc;



#pragma mark - UIAlertController Methods

- (void)showAlertViewWithTitle:(NSString *)title message:(NSString *)message;

- (void)showAlertViewWithTitle:(NSString *)title message:(NSString *)message withDismissCompletion:(AlertViewDismissHandler)dismissHandler;

- (void)showConfirmationAlertViewWithTitle:(NSString *)title message:(NSString *)message withDismissCompletion:(AlertViewConfirmedButtonClickedHandler)dismissHandler;

- (void)showCurrentPasswordConfirmationAlertViewWithTitle:(NSString *)title message:(NSString *)message withDismissCompletion:(AlertViewCurrentPasswordConfirmedHandler)dismissHandler;

- (void)showPINConfirmationAlertViewWithDismissCompletion:(AlertViewCurrentPasswordConfirmedHandler)dismissHandler;


- (void)showActionSheetForImageInputWithCompletionHandler:(AlertViewButtonClickedAtIndexHandler)completionHandler;

- (void)showActionSheetWithPickerView:(UIPickerView *)pickerView titleMessage:(NSString *)title completionHandler:(AlertViewDismissHandler)completionHandler;

@end
