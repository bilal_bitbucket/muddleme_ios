//
//  ImportContactsViewController.m
//  Muddleme
//
//  Created by Asad Ali on 09/08/2015.
//  Copyright (c) 2015 AESquares. All rights reserved.
//

#import "ImportContactsViewController.h"
#import "MyAccountTableViewCell.h"

@interface ImportContactsViewController ()
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UITableView *tblAccountDetails;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@end

@implementation ImportContactsViewController



#pragma mark - UIViewController Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    
    [self setupNavigationBarWithMenuForTitle:@"Import Contacts"];
   // self.tblAccountDetails.tableFooterView = [UIView new];
    
    [self.containerView roundTheCornersWithBorder:4.0];
    self.textField.layer.cornerRadius = 5;
    self.textField.layer.borderWidth = 1;
    self.textField.layer.borderColor = [UIColor blueColor].CGColor;

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}



#pragma mark - Buttons Action

#pragma mark - UITableView DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 8;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"MyAccountTableViewCell";
    
    MyAccountTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    [cell removeCellSeparatorOffset];
    
    
    return cell;
}

@end
