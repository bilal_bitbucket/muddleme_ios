//
//  LoyaltyProgramPicker2ViewController.m
//  Muddleme
//
//  Created by Arkhitech on 22/12/2015.
//  Copyright © 2015 AESquares. All rights reserved.
//

#import "LoyaltyProgramPicker2ViewController.h"

#import "nuHelpers.h"
#import "UIImageView+WebCache.h"
#import "MerchantOffersViewController.h"
#import "SVHTTPClient.h"
#import "AuctionFeedbackViewController.h"

@interface LoyaltyProgramPicker2ViewController ()
@property (weak, nonatomic) IBOutlet UICollectionView *homeCollectionView;
@property (weak, nonatomic) IBOutlet UIView *containerView;


@property (strong, nonatomic) NSMutableArray *collectionItems;
@property (strong, nonatomic) UIRefreshControl *refreshControl;
@end

@implementation LoyaltyProgramPicker2ViewController

#pragma mark - UIViewController Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupNavigationBarWithMenuForTitle:@"Loyality Program"];
    
    [self.containerView roundTheCorners:5.0];
    
    [self getAvailableMerchants];
    
    
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    
    self.refreshControl.tintColor = [UIColor grayColor];
    [self.refreshControl addTarget:self action:@selector(getAvailableMerchants) forControlEvents:UIControlEventValueChanged];
    
    [self.homeCollectionView addSubview:self.refreshControl];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}





#pragma mark - Buttons Action


- (void)getAvailableMerchants
{
    [SVProgressHUD show];
    
    [[UtilityFunctions authenticatedHTTPClient] GET:@"users/loyalty_programs.json" parameters:nil completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error)
     {
         [SVProgressHUD dismiss];
         
         if (!error)
         {
             
             self.collectionItems = [[NSMutableArray alloc ]init];
             for (NSDictionary* dict in response)  {
                 
                 LoyaltyProgram* programm = [[LoyaltyProgram alloc] init];
                 [programm InintWithDic2:dict];

                 if (programm.CoupansCount>0)
                 {
                     [self.collectionItems addObject:programm];

                 }
                 
                 
             }
             
             [self.homeCollectionView reloadData];
         }
         else
         {
             [self showAlertViewWithTitle:kEmptyString message:error.localizedDescription];
         }
     }];
}




#pragma mark - UICollectionView DataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.collectionItems.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"HomeCollectionViewCell";
    
    LoyaltyProgram *itemDict = self.collectionItems[indexPath.row];
    
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    
    UIView *containerView = (UIView *)[cell viewWithTag:2];
    containerView.backgroundColor = [UIColor whiteColor];
    containerView.layer.cornerRadius = 5.0;
    
    NSString* url = [NSString stringWithFormat:@"http://104.197.68.95/%@",itemDict.image];
    
    UIImageView *logoImageView = (UIImageView *)[cell viewWithTag:1];
    [logoImageView sd_setImageWithURL:[NSURL URLWithString:url] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
    }];
    
    
    
    
    
    return cell;
}



#pragma mark - UICollectionView FlowLayout Delegate

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(self.containerView.width/3, self.containerView.width/3);//self.containerView.height/5 - 8);
}



#pragma mark - UICollectionView Delegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self.view endEditing:YES];
    
    
    selectedIndex = indexPath.row;
    
    MerchantOffersViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"MerchantOffersViewController"];
    
    
        [self performSegueWithIdentifier:@"LoyaltyPgmToOffers" sender:self];
    
    
    
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    
    if ([segue.identifier isEqualToString:@"LoyaltyPgmToOffers"])
    {
        MerchantOffersViewController *controller = segue.destinationViewController;
        LoyaltyProgram *itemDict = self.collectionItems[selectedIndex];
        controller.currentProgram = itemDict;
    }
}

@end
