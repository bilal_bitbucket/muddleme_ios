//
//  HomeViewController.m
//  Muddleme
//
//  Created by Asad Ali on 09/08/2015.
//  Copyright (c) 2015 AESquares. All rights reserved.
//

#import "HomeViewController.h"
#import "nuHelpers.h"
#import "UIImageView+WebCache.h"
#import "MerchantOffersViewController.h"
#import "SVHTTPClient.h"
#import "JoinLaoyaltyViewController.h"
#import "AuctionFeedbackViewController.h"


@interface HomeViewController ()<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>


@property (weak, nonatomic) IBOutlet UICollectionView *homeCollectionView;
@property (weak, nonatomic) IBOutlet UIView *containerView;


@property (strong, nonatomic) NSMutableArray *collectionItems;
@property (strong, nonatomic) NSMutableArray *joinedItems;
@property (weak, nonatomic) IBOutlet UITextField *txSearch;

@property (strong, nonatomic) UIRefreshControl *refreshControl;

@end


@implementation HomeViewController



#pragma mark - UIViewController Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    

    [self setupNavigationBarWithMenuForTitle:@"home"];
    
    [self.containerView roundTheCorners:5.0];
    
    [self getAvailableMerchants];
    
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    
    self.txSearch.attributedPlaceholder = PlaceHolderAttributedString(self.txSearch.placeholder);

    
    self.refreshControl.tintColor = [UIColor grayColor];
    [self.refreshControl addTarget:self action:@selector(getAvailableMerchants) forControlEvents:UIControlEventValueChanged];
    
    [self.homeCollectionView addSubview:self.refreshControl];
    // attach long press gesture to collectionView
    UILongPressGestureRecognizer *lpgr
    = [[UILongPressGestureRecognizer alloc]
       initWithTarget:self action:@selector(handleLongPress:)];
    lpgr.delegate = self;
    lpgr.delaysTouchesBegan = YES;
    [self.homeCollectionView addGestureRecognizer:lpgr];
}

-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer.state != UIGestureRecognizerStateEnded) {
        return;
    }
    CGPoint p = [gestureRecognizer locationInView:self.homeCollectionView];
    
    NSIndexPath *indexPath = [self.homeCollectionView indexPathForItemAtPoint:p];
    if (indexPath == nil){
        NSLog(@"couldn't find index path");
    } else {
        longPressed = true;
        
        [self.homeCollectionView reloadData];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

-(void)viewDidAppear:(BOOL)animated
{
//    JoinLaoyaltyViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"JoinLaoyaltyViewController"];
//    
//    // controller.merchantInfo = itemDict;
//    
//    [self.view addSubview:controller.view];
    
    
    
    
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
    
    if (longPressed)
    {
        longPressed = false;
        [self.homeCollectionView reloadData];
        
    }
}


- (void)getAvailableMerchants
{
    [SVProgressHUD show];
    
    [[UtilityFunctions authenticatedHTTPClient] GET:@"loyalty_programs.json" parameters:nil completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error)
     {
         [SVProgressHUD dismiss];
         
         if (!error)
         {
             
             self.collectionItems = [[NSMutableArray alloc ]init];
             for (NSDictionary* dict in response)  {
                 
                 LoyaltyProgram* programm = [[LoyaltyProgram alloc] init];
                 [programm InintWithDic:dict];
                 [self.collectionItems addObject:programm];
             }
             
           //  self.collectionItems = response;
             [self getMyMerchants];
//             [self.homeCollectionView reloadData];
         }
         else
         {
             [self showAlertViewWithTitle:kEmptyString message:error.localizedDescription];
         }
     }];
}


- (void)getMyMerchants
{
    
    
    
    NSMutableDictionary* params = [[NSMutableDictionary alloc] init];
        
    
    [SVProgressHUD show];
    
    [[UtilityFunctions authenticatedHTTPClient] GET:@"users/loyalty_programs.json" parameters:params completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error)
     {
         [SVProgressHUD dismiss];
         
         if (!error)
         {
             self.joinedItems = [[NSMutableArray alloc ]init];

             
             for (NSDictionary* dict in response)  {
                 
                 LoyaltyProgram* programm = [[LoyaltyProgram alloc] init];
                 [programm InintWithDic2:dict];
                 [self.joinedItems addObject:programm];
                 
                 for (LoyaltyProgram* pgm in self.collectionItems)
                 {
                     if(pgm.PId == programm.PId)
                     {
                         pgm.bIsJoined = YES;
                         pgm.account = programm.account;
                         break;
                     }
                 }
             }
             
             [self SortPgms];
             [self.homeCollectionView reloadData];
         }
         else
         {
             [self showAlertViewWithTitle:kEmptyString message:error.localizedDescription];
         }
     }];
}

#pragma mark - Buttons Action





#pragma mark - UICollectionView DataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.collectionItems.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"HomeCollectionViewCell";

    LoyaltyProgram *itemDict = self.collectionItems[indexPath.row];
    
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    
    UIView *containerView = (UIView *)[cell viewWithTag:2];
    containerView.backgroundColor = [UIColor whiteColor];
    containerView.layer.cornerRadius = 5.0;
    
    NSString* url = [NSString stringWithFormat:@"http://104.197.68.95/%@",itemDict.image];
    
    UIImageView *logoImageView = (UIImageView *)[cell viewWithTag:1];
    [logoImageView sd_setImageWithURL:[NSURL URLWithString:url] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
        if (!itemDict.bIsJoined&&image) {
            logoImageView.image = [self convertImageToGrayScale:image];
            NSLog(@"Error Downloading Image: %@", error); }}];


        
   UIImageView *lockImageView = (UIImageView *)[cell viewWithTag:10];
    if (itemDict.bIsJoined)
    {
        lockImageView.hidden = true;
    }else{
        lockImageView.hidden = false;
    }
    
    
    UIView *dltView = (UIView *)[cell viewWithTag:101];
    UIButton* dltButton = dltView.subviews[0];
    dltButton.tag = indexPath.row;
    [dltButton addTarget:self action:@selector(dlt:) forControlEvents:UIControlEventTouchUpInside];
    if (!longPressed)
    {
        dltView.hidden = true;
    }else{
        dltView.hidden = false;
    }

    return cell;
}


-(IBAction)dlt:(id)sender
{
    
    
    NSLog(@"%li",((UIButton*)sender).tag);
    
    selectedIndex = ((UIButton*)sender).tag;
    [self LeaveLoyaltyProgram:nil];
}


#pragma mark - UICollectionView FlowLayout Delegate

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(self.containerView.width/3, self.containerView.width/3);//self.containerView.height/5 - 8);
}




#pragma mark - UICollectionView Delegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self.view endEditing:YES];
    
    selectedIndex = indexPath.row;
    
    LoyaltyProgram *itemDict = self.collectionItems[indexPath.row];
    
    self.loyaltyNamelbl.text = itemDict.Name;
    
//    MerchantOffersViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"MerchantOffersViewController"];
//    
//    controller.merchantInfo = itemDict;
//    
//    [self.navigationController pushViewController:controller animated:YES];
    
    
//    JoinLaoyaltyViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"JoinLaoyaltyViewController"];
//    
//   // controller.merchantInfo = itemDict;
//    
//    [self.view addSubview:controller.view];
    //[self.navigationController pushViewController:controller animated:YES];
    
    [self.joinView setHidden:NO];

}



-(IBAction)goBack:(id)sender
{
    [self.joinView setHidden:YES];
}

- (UIImage *)convertImageToGrayScale:(UIImage *)image {
    
    
    // Create image rectangle with current image width/height
    CGRect imageRect = CGRectMake(0, 0, image.size.width, image.size.height);
    
    // Grayscale color space
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceGray();
    
    // Create bitmap content with current image size and grayscale colorspace
    CGContextRef context = CGBitmapContextCreate(nil, image.size.width, image.size.height, 8, 0, colorSpace, kCGImageAlphaNone);
    
    // Draw image into current context, with specified rectangle
    // using previously defined context (with grayscale colorspace)
    CGContextDrawImage(context, imageRect, [image CGImage]);
    
    // Create bitmap image info from pixel data in current context
    CGImageRef imageRef = CGBitmapContextCreateImage(context);
    
    // Create a new UIImage object
    UIImage *newImage = [UIImage imageWithCGImage:imageRef];
    
    // Release colorspace, context and bitmap information
    CGColorSpaceRelease(colorSpace);
    CGContextRelease(context);
    CFRelease(imageRef);
    
    // Return the new grayscale image
    return newImage; }

-(IBAction)JoinLoyaltyProgram:(id)sender{
    
    
    [self goBack:nil];
    LoyaltyProgram *itemDict = self.collectionItems[selectedIndex];
    
    if (itemDict.bIsJoined)
    {
        return;
    }

    
    
    
    NSString* url = [NSString stringWithFormat:@"user/loyalty_programs/%ld.json",(long)itemDict.PId];
    
    
    
    [SVProgressHUD show];
    
    [[UtilityFunctions authenticatedHTTPClient] POST:url parameters:nil completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error)
     {
         [SVProgressHUD dismiss];
         
         if (!error)
         {
             //self.collectionItems = response;
             itemDict.bIsJoined = true;
             [self SortPgms];

             [self.homeCollectionView reloadData];
         }
         else
         {
             [self showAlertViewWithTitle:kEmptyString message:error.localizedDescription];
         }
     }];

}


-(void)SortPgms
{
    BOOL isUnFollewed = false;
    int totalFollowed = 0;
    
    NSMutableArray* arr = [self.collectionItems mutableCopy];
    for (int i=0;i<arr.count;i++)
    {
        LoyaltyProgram *itemDict = arr[i];
        
        if (itemDict.bIsJoined)
        {
            if (isUnFollewed)
            {
                LoyaltyProgram *itemDict2 = self.collectionItems[i];
                [self.collectionItems removeObject:itemDict2];
                [self.collectionItems insertObject:itemDict2 atIndex:totalFollowed];
            }
            
            totalFollowed++;
        }else
        {
            isUnFollewed = true;
        }
        

    }
}

-(IBAction)LeaveLoyaltyProgram:(id)sender{
    
    [self goBack:nil];
    LoyaltyProgram *itemDict = self.collectionItems[selectedIndex];

    if (!itemDict.bIsJoined)
    {
        return;
    }
  
    
    
    
    
    
    
    NSString* url = [NSString stringWithFormat:@"user/loyalty_programs/%ld.json",(long)itemDict.PId];
    
    
    
    [SVProgressHUD show];
    
    [[UtilityFunctions authenticatedHTTPClient] DELETE:url parameters:nil completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error)
     {
         [SVProgressHUD dismiss];
         
         if (!error)
         {
             //self.collectionItems = response;
             itemDict.bIsJoined = false;
             
             [self.collectionItems removeObjectAtIndex:selectedIndex];

             [self.homeCollectionView reloadData];
         }
         else
         {
             [self showAlertViewWithTitle:kEmptyString message:error.localizedDescription];
         }
     }];
    
}
- (IBAction)ProgramReview:(id)sender {
    [self goBack:nil];

    
    LoyaltyProgram *itemDict = self.collectionItems[selectedIndex];
    
    self.loyaltyNamelbl.text = itemDict.Name;
    
        AuctionFeedbackViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"AuctionFeedbackViewController2"];
    
        controller.currentProgram = itemDict;
    
        [self.navigationController pushViewController:controller animated:YES];

}



<<<<<<< HEAD
=======

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.searchedItems.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"MyAccountTableViewCell";
    
    MyAccountTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    [cell removeCellSeparatorOffset];
    
    NSDictionary* dic = [self.collectionItems objectAtIndex:indexPath.row];
    
    cell.lblTitle.text = dic[@"title"];
    cell.lblValue.text = dic[@"comment"];
    
    return cell;
}






- (void)getSearchResults:(NSString*)string
{
    if ([string isEqualToString:@""])
    {
        [self getAvailableMerchants];
        return;
    }
    
    [SVProgressHUD show];
    NSMutableDictionary* params = [[NSMutableDictionary alloc] init];

    [params setValue:string forKey:@"keyword"];
    
    [[UtilityFunctions authenticatedHTTPClient] GET:@"search_all.json" parameters:params completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error)
     {
         [SVProgressHUD dismiss];
         
         if (!error)
         {
             
             self.collectionItems = [[NSMutableArray alloc ]init];
             
             if (response && ![response isKindOfClass:[NSNull class]])
             {
                 for (NSDictionary* dict in response)  {
                     
                     LoyaltyProgram* programm = [[LoyaltyProgram alloc] init];
                     [programm InintWithDic:dict];
                     [self.collectionItems addObject:programm];
                 }
             }
             
            
             for (LoyaltyProgram* programm in self.joinedItems)
             {
                 for (LoyaltyProgram* pgm in self.collectionItems)
                 {
                     if(pgm.PId == programm.PId)
                     {
                         pgm.bIsJoined = YES;
                         break;
                     }
                 }
             }

             [self.homeCollectionView reloadData];

             
         }
         else
         {
             [self showAlertViewWithTitle:kEmptyString message:error.localizedDescription];
         }
     }];
}





#pragma mark- TextField delegates

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    
    NSString *substring = [NSString stringWithString:textField.text];
    
       substring=[substring stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    substring=[substring stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [self getSearchResults:substring];
    [textField resignFirstResponder];
    
    return YES;
}


>>>>>>> 1b55bff... updated coupan view
@end
