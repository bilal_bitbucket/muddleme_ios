//
//  MenuViewController.m
//  Muddleme
//
//  Created by Asad Ali on 09/08/2015.
//  Copyright (c) 2015 AESquares. All rights reserved.
//

#import "MenuViewController.h"
#import "MenuTableViewCell.h"

#import "HomeViewController.h"
#import "AccountBalanceViewController.h"
#import "MyAccountViewController.h"
#import "OffersViewController.h"
#import "ImportContactsViewController.h"
#import "AuctionArchiveViewController.h"
#import "AuctionFeedbackViewController.h"
#import "MerchantOffersViewController.h"

@interface MenuViewController ()<UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tblMenu;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tblBottomConstrain;

@property (strong, nonatomic) NSArray *menuItems;

@property (strong, nonatomic) HomeViewController *homeVC;
@property (strong, nonatomic) AccountBalanceViewController *balanceVC;
@property (strong, nonatomic) MyAccountViewController *myAccountVC;
@property (strong, nonatomic) MerchantOffersViewController *offersVC;
@property (strong, nonatomic) ImportContactsViewController *importVC;
@property (strong, nonatomic) AuctionArchiveViewController *auctionArchiveVC;
@property (strong, nonatomic) AuctionFeedbackViewController *auctionFeedbackVC;

@end


@implementation MenuViewController



#pragma mark - UIViewController Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    
    
    self.homeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
    self.balanceVC = [self.storyboard instantiateViewControllerWithIdentifier:@"AccountBalanceViewController"];
    self.myAccountVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MyAccountViewController"];
    self.offersVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LoyaltyProgramPicker2ViewController"];
    self.importVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ImportContactsViewController"];
    self.auctionArchiveVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LoyaltyProgramPickerViewController3"];
    self.auctionFeedbackVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LoyaltyProgramPickerViewController"];
    
    
    self.menuItems = @[@[@"user-icon", @"Your Account", @"View, edit and update your account data", self.myAccountVC],
                       @[@"account-icon", @"Account Balance", @"Check and withdraw your earnings", self.balanceVC],
                       @[@"coupon-icon", @"Coupons and Deals", @"View all offers and deals ​from merchants", self.offersVC],
                       @[@"personal-icon", @"Personal Offer Archive", @"View auction, vendor and offer history", self.auctionArchiveVC],
                       @[@"thumb-up-icon", @"Provide Loyalty feedback", @"Confirm your transactions and get paid", self.auctionFeedbackVC],
                       @[@"sheare-icon", @"Import and share contacts", @"Share MuddleMe with friends and earn even more $", self.importVC],
                       @[@"logout-icon", @"Logout", @"If this isn’t your account", @(0)]];
    
    
    [self hideNavigationBar:YES];
    

    [self addChildViewControllerInContainer:self.containerView childViewController:self.homeVC];
    
    [self hideMenuAnimated:NO];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)hideMenuAnimated:(BOOL)animated
{
    self.tblBottomConstrain.constant = self.tblBottomConstrain.constant < 0? 0:-(self.view.height-64);
    
    if (animated)
    {
        [UIView animateWithDuration:kAnimationDuration animations:^{

            [self.tblMenu layoutIfNeeded];
        }];
    }
}



#pragma mark - Buttons Action



#pragma mark - UITableView DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.menuItems.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return (self.view.height-64)/self.menuItems.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"MenuTableViewCell";
    
    MenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    
    NSArray *menuItem = self.menuItems[indexPath.row];
    
    cell.imgLogo.image = UIImageFromName(menuItem[0]);
    cell.lblHeading.text = menuItem[1];
    cell.lblSubtitle.text = menuItem[2];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < self.menuItems.count-1)
    {
        NSArray *menuItem = self.menuItems[indexPath.row];
        
        
        [self addChildViewControllerInContainer:self.containerView childViewController:menuItem[3]];
        
        [self hideMenuAnimated:YES];
    }
    else
    {
        // It's logout
        [self logoutToLoginViewController];
    }
}


@end
