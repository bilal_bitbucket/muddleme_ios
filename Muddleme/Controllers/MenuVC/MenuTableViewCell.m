//
//  MenuTableViewCell.m
//  Muddleme
//
//  Created by Asad Ali on 09/08/2015.
//  Copyright (c) 2015 AESquares. All rights reserved.
//

#import "MenuTableViewCell.h"
#import "nuHelpers.h"

@implementation MenuTableViewCell


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    self.imgBg.image = UIImageFromName(selected? @"menu_selected_bg":@"menu_normal_bg");
}

@end
