//
//  LandingViewController.m
//  Muddleme
//
//  Created by Asad Ali on 09/08/2015.
//  Copyright (c) 2015 AESquares. All rights reserved.
//

#import "LandingViewController.h"


@interface LandingViewController ()

@end

@implementation LandingViewController



#pragma mark - UIViewController Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    if ([UtilityFunctions getValueFromUserDefaultsForKey:@"user"])
    {
        [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"MenuViewController"] animated:NO];
    }
    else
    {
        [self hideNavigationBar:YES];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self hideNavigationBar:YES animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}



#pragma mark - Buttons Action



@end
