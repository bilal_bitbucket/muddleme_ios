//
//  ScanQRViewController.h
//  Muddleme
//
//  Created by Arkhitech on 14/12/2015.
//  Copyright © 2015 AESquares. All rights reserved.
//

#import "BaseViewController.h"
#import "MTBBarcodeScanner.h"

@protocol ScanQRViewControllerDelegate

-(void)ImageCaptured:(UIImage*)qrImage;

@end

@interface ScanQRViewController : BaseViewController
{
    MTBBarcodeScanner* scanner;
}
@property (weak, nonatomic) IBOutlet UIImageView *bgView2;
@property (nonatomic,strong)  id<ScanQRViewControllerDelegate> delegate;

@end
