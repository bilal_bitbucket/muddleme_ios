//
//  addMerchantViewController.m
//  Muddleme
//
//  Created by Arkhitech on 11/12/2015.
//  Copyright © 2015 AESquares. All rights reserved.
//

#import "addMerchantViewController.h"

@interface addMerchantViewController ()
@property (weak, nonatomic) IBOutlet UIView *containerView;

@end

@implementation addMerchantViewController



#pragma mark - UIViewController Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    
    [self setupNavigationBarWithTitle:@"Add merchant loyalty program" showBackButtonIfNeeded:YES];
    [self.containerView roundTheCornersWithBorder:4.0];
    self.textFieldName.layer.cornerRadius = 5;
    self.textFieldName.layer.borderWidth = 1;
    self.textFieldName.layer.borderColor = [UIColor whiteColor].CGColor;
    
    self.textFieldID.layer.cornerRadius = 5;
    self.textFieldID.layer.borderWidth = 1;
    self.textFieldID.layer.borderColor = [UIColor whiteColor].CGColor;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}



#pragma mark - Buttons Action


@end


