//
//  addMerchantViewController.h
//  Muddleme
//
//  Created by Arkhitech on 11/12/2015.
//  Copyright © 2015 AESquares. All rights reserved.
//

#import "BaseViewController.h"

@interface addMerchantViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UIView *bgView1;
@property (weak, nonatomic) IBOutlet UIView *bgView2;
@property (weak, nonatomic) IBOutlet UIView *bgView3;
@property (weak, nonatomic) IBOutlet UIView *bgView4;
@property (weak, nonatomic) IBOutlet UIView *bgView5;
@property (weak, nonatomic) IBOutlet UITextField *textFieldName;
@property (weak, nonatomic) IBOutlet UITextField *textFieldID;

@end
