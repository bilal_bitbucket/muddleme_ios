//
//  MyAccountViewController.m
//  Muddleme
//
//  Created by Asad Ali on 09/08/2015.
//  Copyright (c) 2015 AESquares. All rights reserved.
//

#import "MyAccountViewController.h"
#import "MyAccountTableViewCell.h"
#import "Muddleme-Swift.h"


@interface MyAccountViewController () <UITableViewDataSource, UITableViewDelegate>


@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UITableView *tblAccountDetails;

@property (weak, nonatomic) IBOutlet UIView *LblView;
@property (weak, nonatomic) IBOutlet UILabel *LblScore;


@end

@implementation MyAccountViewController



#pragma mark - UIViewController Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
        
    
    [self setupNavigationBarWithMenuForTitle:@"Your Account"];
    
    self.tblAccountDetails.tableFooterView = [UIView new];
    
    [self.containerView roundTheCornersWithBorder:4.0];
    [self.LblView roundTheCornersWithBorder:4.0];
    
    [self getAvailableScore];

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}



- (void)getAvailableScore
{
    [SVProgressHUD show];
    NSDictionary* dict = [UtilityFunctions getValueFromUserDefaultsForKey:@"user"];
    
    NSMutableDictionary* params = [[NSMutableDictionary alloc] init];
    
    [params setObject:dict[@"id"] forKey:@"user_id"];
    
    [[UtilityFunctions authenticatedHTTPClient] GET:@"score.json" parameters:params completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error)
     {
         [SVProgressHUD dismiss];
         
         if (!error)
         {
             self.LblScore.text = [NSString stringWithFormat:@"%@",response];
             
         }
         else
         {
             [self showAlertViewWithTitle:kEmptyString message:error.localizedDescription];
         }
     }];
}
#pragma mark - Buttons Action

- (IBAction)btnEditAccountClicked:(id)sender
{
}



#pragma mark - UITableView DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 7;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"MyAccountTableViewCell";
    
    MyAccountTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    [cell removeCellSeparatorOffset];
    
    User * user = [User sharedInstance];
    
     if (indexPath.row==0)
    {
        cell.TFValue.text = user.firstName;
        cell.lblTitle.text = @"First Name";
    }else if (indexPath.row==1)
    {
        cell.TFValue.text = user.lastName;
        cell.lblTitle.text = @"Last Name";
    }else if (indexPath.row==2)
    {
        cell.TFValue.text = user.email;
        cell.lblTitle.text = @"Email";
    }else if (indexPath.row==3)
    {
        cell.TFValue.text = user.address;
        cell.lblTitle.text = @"Address";
    }else if (indexPath.row==4)
    {
        cell.TFValue.text = user.city;
        cell.lblTitle.text = @"city";
    }else if (indexPath.row==5)
    {
        cell.TFValue.text = user.zipCode;
        cell.lblTitle.text = @"zip code";
    }
    else if (indexPath.row==6)
    {
        cell.TFValue.text = user.zipCode;
        cell.lblTitle.text = @"Mobile Number";
    }
//    else if (indexPath.row==0)
//    {
//        cell.TFValue.text = user.firstName;
//        cell.lblTitle.text = @"First Name:";
//    }
    
    
    return cell;
}


@end
