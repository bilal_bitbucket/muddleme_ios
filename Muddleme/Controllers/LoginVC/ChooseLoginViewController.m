//
//  ChooseLoginViewController.m
//  Muddleme
//
//  Created by Asad Ali on 09/08/2015.
//  Copyright (c) 2015 AESquares. All rights reserved.
//

#import "ChooseLoginViewController.h"
#import "nuHelpers.h"
//#import <SCFacebook/SCFacebook.h>
#import <SCTwitter/SCTwitter.h>
#import "SCFacebook.h"
#import <Google/SignIn.h>

@interface ChooseLoginViewController ()


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnFbTopConstrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnTwitterTopConstrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnGooglePlusTopConstrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnEmailTopConstrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lineBottomConstrain;


@property (assign, nonatomic) BOOL isViewDidLayoutSubviews;

@end

@implementation ChooseLoginViewController



#pragma mark - UIViewController Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    [self setupNavigationBarWithTitle:@"login" showBackButtonIfNeeded:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidLayoutSubviews
{
    if (!self.isViewDidLayoutSubviews)
    {
        if ([SizeClass currentDeviceSizes].isDeviceIPhone5 || [SizeClass currentDeviceSizes].isDeviceIPhone4)
        {
            self.btnFbTopConstrain.constant = 5.0;
            self.btnTwitterTopConstrain.constant = 8.0;
            self.btnGooglePlusTopConstrain.constant = 8.0;
            self.btnTwitterTopConstrain.constant = 8.0;
            self.btnEmailTopConstrain.constant = 8.0;
            self.lineBottomConstrain.constant = 2.0;
        }
    }
    
    self.isViewDidLayoutSubviews = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}



#pragma mark - Buttons Action

- (IBAction)btnFacebookLoginClicked:(id)sender
{
    
    [SCFacebook loginWithBehavior:FBSDKLoginBehaviorWeb CallBack:^(BOOL success, id result) {
        if (success) {
            
            [SVProgressHUD show];
            
            
            FBSDKAccessToken* token = ((FBSDKLoginManagerLoginResult*)result).token;
            
            
            FBSDKGraphRequest* request = [[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @" email"}];
            [request startWithCompletionHandler:^(FBSDKGraphRequestConnection* conn, id result,NSError* eror){
                
                
                
                            NSMutableDictionary* params = [[NSMutableDictionary alloc] init];
                
                            [params setObject:result[@"email"] forKey:@"email"];
                            [params setObject:token.userID forKey:@"uid"];
                            [params setObject:token.tokenString forKey:@"access_token"];
                            [params setObject:@"facebook" forKey:@"provider"];
                
                
                            [[SVHTTPClient sharedClient] POST:@"omniauth_login.json" parameters:params completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error)
                             {
                                 [SVProgressHUD dismiss];
                
                                 NSLog(@"response: %@", response);
                
                                 if ([response isKindOfClass:[NSDictionary class]])
                                 {
                                     if (response[@"error"])
                                     {
                                         [self showAlertViewWithTitle:kEmptyString message:response[@"error"]];
                
                
                                     }else{
                                         User * user = [User sharedInstance];
                                         [user InintWithDic:response];
                                         
                                         NSDictionary *userInfo = @{@"email":response[@"email"], @"id":response[@"id"],@"access_token":response[@"access_token"], @"first_name":[NSString stringWithFormat:@"%@ %@", response[@"first_name"], response[@"last_name"]], @"balance":response[@"balance"], @"total_balance":response[@"total_balance"], @"pending_outcome":response[@"pending_outcome"]};
                                         
                                         [UtilityFunctions setValueInUserDefaults:userInfo forKey:@"user"];
                                         
                                         [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"MenuViewController"] animated:YES];
                                     }
                                 }
                                 else
                                 {
                                     [self showAlertViewWithTitle:kEmptyString message:@"Email address or Password is incorrect"];
                                 }
                             }];
                            
                
            }];
            
        }else{
            
        }
    }];
    
}

- (IBAction)btnTwitterLoginClicked:(id)sender
{
    [SCTwitter loginViewControler:self callback:^(BOOL success, id result) {
        if (success) {
            
            
            
            
        }else{
            
        }
    }];
    
}

- (IBAction)btnGoogleLoginClicked:(id)sender
{
    [GIDSignIn sharedInstance].allowsSignInWithWebView = YES;
    [GIDSignIn sharedInstance].uiDelegate = self;
    [GIDSignIn sharedInstance].delegate = self;
    [[GIDSignIn sharedInstance] signIn];
}
// [START signin_handler]
- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    
    
    if (!error)
    {
        [SVProgressHUD show];
        
        
        NSMutableDictionary* params = [[NSMutableDictionary alloc] init];
        
        [params setObject:user.profile.email forKey:@"email"];
        [params setObject:user.userID forKey:@"uid"];
        [params setObject:user.authentication.idToken forKey:@"access_token"];
        [params setObject:@"google" forKey:@"provider"];
        
        
        [[SVHTTPClient sharedClient] POST:@"omniauth_login.json" parameters:params completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error)
         {
             
             [SVProgressHUD dismiss];
             
             NSLog(@"response: %@", response);
             
             if ([response isKindOfClass:[NSDictionary class]])
             {
                 if (response[@"error"])
                 {
                     [self showAlertViewWithTitle:kEmptyString message:@"Email address or Password is incorrect"];
                     
                     
                 }else{
                     User * user = [User sharedInstance];
                     [user InintWithDic:response];
                     
                     NSDictionary *userInfo = @{@"email":response[@"email"], @"id":response[@"id"],@"access_token":response[@"access_token"], @"first_name":[NSString stringWithFormat:@"%@ %@", response[@"first_name"], response[@"last_name"]], @"balance":response[@"balance"], @"total_balance":response[@"total_balance"], @"pending_outcome":response[@"pending_outcome"]};
                     
                     [UtilityFunctions setValueInUserDefaults:userInfo forKey:@"user"];
                     
                     [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"MenuViewController"] animated:YES];
                     
                     
                     
                 }
             }
             else
             {
                 [self showAlertViewWithTitle:kEmptyString message:@"Email address or Password is incorrect"];
             }
         }];
        
        
    }
    
    
    
    
    
    
    
    // Perform any operations on signed in user here.
}
// [END signin_handler]

// This callback is triggered after the disconnect call that revokes data
// access to the user's resources has completed.
// [START disconnect_handler]
- (void)signIn:(GIDSignIn *)signIn
didDisconnectWithUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    // Perform any operations when the user disconnects from app here.
    
}
// [END disconnect_handler]




- (IBAction)btnPrivacyPolicyClicked:(id)sender
{
}

- (IBAction)btnTermsOfService:(id)sender
{
}


@end
