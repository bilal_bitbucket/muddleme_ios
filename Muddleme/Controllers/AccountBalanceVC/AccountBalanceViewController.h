//
//  AccountBalanceViewController.h
//  Muddleme
//
//  Created by Asad Ali on 09/08/2015.
//  Copyright (c) 2015 AESquares. All rights reserved.
//

#import "BaseViewController.h"

@interface AccountBalanceViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UIView *bgView1;
@property (weak, nonatomic) IBOutlet UIView *bgView2;
@property (weak, nonatomic) IBOutlet UIView *bgView3;
@property (weak, nonatomic) IBOutlet UIView *bgView4;
@property (weak, nonatomic) IBOutlet UIView *bgView5;

@end
