//
//  AuctionArchiveViewController.h
//  Muddleme
//
//  Created by Asad Ali on 09/08/2015.
//  Copyright (c) 2015 AESquares. All rights reserved.
//

#import "BaseViewController.h"

@interface AuctionArchiveViewController : BaseViewController
@property (weak, nonatomic)  LoyaltyProgram *currentProgram;

@end
