//
//  MerchantOffersViewController.h
//  Muddleme
//
//  Created by Asad Ali on 16/09/2015.
//  Copyright (c) 2015 AESquares. All rights reserved.
//

#import "BaseViewController.h"

@interface MerchantOffersViewController : BaseViewController


@property (strong, nonatomic) NSDictionary *merchantInfo;
@property (weak, nonatomic)  LoyaltyProgram *currentProgram;


@end
