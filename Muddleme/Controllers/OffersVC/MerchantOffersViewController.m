//
//  MerchantOffersViewController.m
//  Muddleme
//
//  Created by Asad Ali on 16/09/2015.
//  Copyright (c) 2015 AESquares. All rights reserved.
//

#import "MerchantOffersViewController.h"
#import "OfferCollectionViewCell.h"
#import "AuctionArchiveViewController.h"

@interface MerchantOffersViewController ()<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>


@property (weak, nonatomic) IBOutlet UILabel *lblMerchantName;
@property (weak, nonatomic) IBOutlet UILabel *lblUserFullName;
@property (weak, nonatomic) IBOutlet UIView *headView;
@property (weak, nonatomic) IBOutlet UICollectionView *offersCollectionView;
@property (weak, nonatomic) IBOutlet UILabel *lblAccountNumber;


@property (strong, nonatomic) NSMutableArray *offers;


@end

@implementation MerchantOffersViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setTranslucent:NO];
    
    //[self setupNavigationBarWithMenuForTitle:@"Muddle M"];
    [self setupNavigationBarWithTitle:@"Muddle M" showBackButtonIfNeeded:YES];

    
    self.headView.layer.cornerRadius = 5.0;
    
    [self.offersCollectionView registerNib:[UINib nibWithNibName:@"OfferCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"OfferCollectionViewCell"];
    

    
    
    NSDictionary *userInfo = [UtilityFunctions getValueFromUserDefaultsForKey:@"user"];
    
    
    self.lblUserFullName.text = userInfo[@"first_name"];
    self.lblMerchantName.text = self.currentProgram.Name;
    self.lblAccountNumber.text = self.currentProgram.account;
    //self.lblAccountNumber.text =userInfo[@"email"];
    
    if (self.bIsFromHome)
    {
        [self getOffersForAffiliates:self.merchantInfo[@"advertiser_id"]];

    }else
    {
        [self getOffersForMerchant:self.merchantInfo[@"advertiser_id"]];

    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)getOffersForMerchant:(NSString *)merchantId
{
    [SVProgressHUD show];
    NSMutableDictionary* params = [[NSMutableDictionary alloc] init];
    
    [params setObject:[NSString stringWithFormat:@"%li",self.currentProgram.PId] forKey:@"loyalty_program_id"];
    
    NSString* url = [NSString stringWithFormat:@"/loyalty_programs/%li/coupons",self.currentProgram.PId];
    
    [[UtilityFunctions authenticatedHTTPClient] GET:url parameters:nil completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error)
     {
         [SVProgressHUD dismiss];
         self.offers = [[NSMutableArray alloc ]init];

         
         NSArray* arr1 = [response valueForKey:@"avant_coupons"];
         for (NSDictionary* dict in arr1)  {
             
             Coupon* programm = [[Coupon alloc] init];
             [programm InintWithDic:dict];
             [self.offers addObject:programm];
         }
         NSArray* arr2 = [response valueForKey:@"cj_coupons"];
         for (NSDictionary* dict in arr2)  {
             
             Coupon* programm = [[Coupon alloc] init];
             [programm InintWithDic:dict];
             [self.offers addObject:programm];
         }
         NSArray* arr3 = [response valueForKey:@"ir_coupons"];
         for (NSDictionary* dict in arr3)  {
             
             Coupon* programm = [[Coupon alloc] init];
             [programm InintWithDic:dict];
             [self.offers addObject:programm];
         }
         NSArray* arr4 = [response valueForKey:@"linkshare_coupons"];
         for (NSDictionary* dict in arr4)  {
             
             Coupon* programm = [[Coupon alloc] init];
             [programm InintWithDic:dict];
             [self.offers addObject:programm];
         }
         NSArray* arr5 = [response valueForKey:@"loyalty_program_coupons"];
         for (NSDictionary* dict in arr5)  {
             
             Coupon* programm = [[Coupon alloc] init];
             [programm InintWithDic:dict];
             [self.offers addObject:programm];
         }
         
         NSArray* arr6 = [response valueForKey:@"pj_coupons"];
         for (NSDictionary* dict in arr6)  {
             
             Coupon* programm = [[Coupon alloc] init];
             [programm InintWithDic:dict];
             [self.offers addObject:programm];
         }

         if (self.offers.count > 0) [self.offersCollectionView reloadData];
         else [self showAlertViewWithTitle:kEmptyString message:@"No coupon found"];
     }];
}




- (void)getOffersForAffiliates:(NSString *)merchantId
{
    [SVProgressHUD show];
    NSMutableDictionary* params = [[NSMutableDictionary alloc] init];
    
    [params setObject:[NSString stringWithFormat:@"%li",self.currentProgram.PId] forKey:@"loyalty_program_id"];
    
    NSString* url = [NSString stringWithFormat:@"/loyalty_programs/%li/affiliates_offers",self.currentProgram.PId];
    
    [[UtilityFunctions authenticatedHTTPClient] GET:url parameters:nil completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error)
     {
         [SVProgressHUD dismiss];
         self.offers = [[NSMutableArray alloc ]init];
         
         
         NSArray* arr1 = [response valueForKey:@"avant_offers"];
         for (NSDictionary* dict in arr1)  {
             
             Coupon* programm = [[Coupon alloc] init];
             [programm InintWithDic:dict];
             [self.offers addObject:programm];
         }
         NSArray* arr2 = [response valueForKey:@"cj_offers"];
         for (NSDictionary* dict in arr2)  {
             
             Coupon* programm = [[Coupon alloc] init];
             [programm InintWithDic:dict];
             [self.offers addObject:programm];
         }
         NSArray* arr3 = [response valueForKey:@"linkshare_coupons"];
         for (NSDictionary* dict in arr3)  {
             
             Coupon* programm = [[Coupon alloc] init];
             [programm InintWithDic:dict];
             [self.offers addObject:programm];
         }
         if (self.offers.count > 0) [self.offersCollectionView reloadData];
         else [self showAlertViewWithTitle:kEmptyString message:@"No Offers found"];
     }];
}
#pragma mark - UICollectionViewDataSource

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    OfferCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"OfferCollectionViewCell" forIndexPath:indexPath];
    
    cell.cellData = self.offers[indexPath.row];
    
    cell.layer.cornerRadius = 5.0;
    cell.delegate = self;
    return cell;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.offers.count;
}



#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(self.offersCollectionView.bounds.size.width, 264);
}


-(void)EnableLocation
{
    [self performSegueWithIdentifier:@"EnableLocation" sender:self];
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    
    if ([segue.identifier isEqualToString:@"CoupansToOffers"])
    {
        AuctionArchiveViewController *controller = segue.destinationViewController;
       // LoyaltyProgram *itemDict = self.collectionItems[selectedIndex];
        controller.currentProgram = self.currentProgram;
    }
}
@end
