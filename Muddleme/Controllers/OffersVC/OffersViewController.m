//
//  OffersViewController.m
//  Muddleme
//
//  Created by Asad Ali on 09/08/2015.
//  Copyright (c) 2015 AESquares. All rights reserved.
//

#import "OffersViewController.h"

@interface OffersViewController ()
@property (strong, nonatomic) NSMutableArray *collectionItems;

@end

@implementation OffersViewController



#pragma mark - UIViewController Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    
    [self setupNavigationBarWithMenuForTitle:@"Offers"];
    
    [self getAvailableCoupans];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}



#pragma mark - Buttons Action


- (void)getAvailableCoupans
{
    [SVProgressHUD show];
    
    [[UtilityFunctions authenticatedHTTPClient] GET:@"coupons.json" parameters:nil completion:^(id response, NSHTTPURLResponse *urlResponse, NSError *error)
     {
         [SVProgressHUD dismiss];
         
         if (!error)
         {
             
             self.collectionItems = [[NSMutableArray alloc ]init];
             for (NSDictionary* dict in response)  {
                 
                 Coupon* programm = [[Coupon alloc] init];
                 [programm InintWithDic:dict];
                 [self.collectionItems addObject:programm];
             }
             
             //  self.collectionItems = response;
             //             [self.homeCollectionView reloadData];
         }
         else
         {
             [self showAlertViewWithTitle:kEmptyString message:error.localizedDescription];
         }
     }];
}


#pragma mark - UICollectionView DataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.collectionItems.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"HomeCollectionViewCell";
    
    LoyaltyProgram *itemDict = self.collectionItems[indexPath.row];
    
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    
    UIView *containerView = (UIView *)[cell viewWithTag:2];
    containerView.backgroundColor = [UIColor whiteColor];
    containerView.layer.cornerRadius = 5.0;
    
    NSString* url = [NSString stringWithFormat:@"http://104.197.68.95/%@",itemDict.image];
    
    UIImageView *logoImageView = (UIImageView *)[cell viewWithTag:1];
    [logoImageView sd_setImageWithURL:[NSURL URLWithString:url] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
       }];
    
    
    
    UIImageView *lockImageView = (UIImageView *)[cell viewWithTag:10];    if (itemDict.bIsJoined)
    {
        lockImageView.hidden = true;
    }else{
        lockImageView.hidden = false;
    }
    
    return cell;
}


@end
