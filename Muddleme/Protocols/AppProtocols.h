//
//  AppProtocols.h
//
//  Created by Asad Ali on 19/03/2015.
//  Copyright (c) 2015 AESquares. All rights reserved.
//

#import <Foundation/Foundation.h>



/*
 
 BaseViewController Delegate Methods for Navigation Button Click Events
 
 */
@protocol BaseViewControllerDelegate <NSObject>


@optional

- (void)rightNavigationBarButtonClicked;

- (void)leftNavigationBarButtonClicked;

- (void)topSwipeDownGestureRecognized;



@end




/*
 
 Scrolling Protocols for Brands View, where Brand logo hides when user scrolls the collectionViews
 
 */
@protocol CollectionViewScrollingProtocol <NSObject>


- (void)collectionViewWillBeginDragging:(UIScrollView *)scrollView;

- (void)collectionViewDidEndDecelerating:(UIScrollView *)scrollView;

- (void)collectionViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate;

- (void)collectionViewDidScroll:(UIScrollView *)collectionView;

@end




@class BaseCollectionView;


/*
 
 Home CollectionView Delegate
 
 */
@protocol BaseCollectionViewDelegate <NSObject>

@optional

- (void)baseCollectionView:(BaseCollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath;

- (void)collectionViewWillBeginDragging:(BaseCollectionView *)collectionView;

- (void)collectionViewDidEndDecelerating:(BaseCollectionView *)collectionView;

- (void)collectionViewDidEndDragging:(BaseCollectionView *)collectionView willDecelerate:(BOOL)decelerate;

- (void)collectionViewDidScroll:(BaseCollectionView *)collectionView;

- (void)collectionViewRefreshDataTriggered:(BaseCollectionView *)collectionView;

- (void)collectionViewRefreshDataLocallyTriggered:(BaseCollectionView *)collectionView;


@end




/*
 
 Home CollectionView Data Source
 
 */
@protocol BaseCollectionViewDataSource <NSObject>


- (NSInteger)baseCollectionView:(BaseCollectionView *)collectionView numberOfItemsInSection:(NSInteger)section;

- (id)baseCollectionView:(BaseCollectionView *)collectionView dataForItemAtIndexPath:(NSIndexPath *)indexPath;

@optional
- (UIViewController *)viewControllerThatContains:(BaseCollectionView *)collectionView;


@end