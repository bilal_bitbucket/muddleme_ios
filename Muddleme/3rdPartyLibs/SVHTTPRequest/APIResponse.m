//
//  APIResponse.m
//
//  Created on 27/09/2013.

//

#import "APIResponse.h"
#import "nuHelpers.h"

@implementation APIResponse

/**
 ** A common method to handle the response.
 ** @dictResponse is the response dictionary that needs to be parsed.
 ** properties @error and @data are populated based on the response.
 **/

+ (APIResponse *)handleResponse:(NSDictionary *)dictResponse error:(NSError *)networkError
{
    APIResponse *response = [[APIResponse alloc] init];
    
    if (!networkError)
    {
        NSDictionary *result = dictResponse;
        
        if ([[result objectForKey:@"status"] intValue] == 0)
        {
            response.error = [[Error alloc] initWithDictionary:result];
            response.data = nil;
        }
        else
        {
            response.error = nil;
            response.optionalMessage = [result objectForKeyNotNull:@"message"];
            response.data = [dictResponse objectForKey:@"results"];
        }
    }
    else
    {
        response.error = [[Error alloc] initWithError:networkError];
        response.data = nil;
    }
    
    return response;
}

@end

