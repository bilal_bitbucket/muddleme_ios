//
//  Coupon.swift
//  Muddleme
//
//  Created by Arkhitech on 21/12/2015.
//  Copyright © 2015 AESquares. All rights reserved.
//

import UIKit

class Coupon: NSObject {

    
    var advertiser_name:String=String()
    var ad_url:String=String()
    var PId:Int=Int()
    
    var ad_id:String=String()
    var CoupondDescription:String=String()
    var expires_at:String=String()
    var header:String=String()
    var code:String=String()
    
    
    func InintWithDic(dict:NSDictionary) {
        
        
        
        
        if(self.valueForKeyExist(dict, key: "advertiser_name") == true){
            self.advertiser_name = dict.valueForKey("advertiser_name") as! String
        }
        if(self.valueForKeyExist(dict, key: "ad_url") == true){
            
            self.ad_url = dict.valueForKey("ad_url") as! String
        }
        if(self.valueForKeyExist(dict, key: "id") == true){
            self.PId = (dict.valueForKey("id") as! Int)
        }

        
        if(self.valueForKeyExist(dict, key: "description") == true){
            self.CoupondDescription = dict.valueForKey("description") as! String
        }
        if(self.valueForKeyExist(dict, key: "expires_at") == true){
            
            self.expires_at = dict.valueForKey("expires_at") as! String
        }
        if(self.valueForKeyExist(dict, key: "code") == true){
            
            self.code = dict.valueForKey("code") as! String
        }
        if(self.valueForKeyExist(dict, key: "header") == true){
            
            self.header = dict.valueForKey("header") as! String
        }
        
        if(self.valueForKeyExist(dict, key: "ad_id") == true){
            self.ad_id = (dict.valueForKey("ad_id") as! String)
        }

    }
    
    
    func valueForKeyExist(dict:NSDictionary, key:String)->Bool
    {
        if(dict.valueForKey(key) != nil)
        {
            let value:AnyObject = dict.valueForKey(key)!
            if(value.isKindOfClass(NSNull) == true)
            {
                return false
            }
            return true
        }
        
        return false
    }
    

    
    func InintWithOfferDic(dict:NSDictionary) {
        
        
        
        
        if(self.valueForKeyExist(dict, key: "advertiser_name") == true){
            self.advertiser_name = dict.valueForKey("advertiser_name") as! String
        }
        if(self.valueForKeyExist(dict, key: "ad_url") == true){
            
            self.ad_url = dict.valueForKey("ad_url") as! String
        }
        if(self.valueForKeyExist(dict, key: "id") == true){
            self.PId = (dict.valueForKey("id") as! Int)
        }
        
        
        if(self.valueForKeyExist(dict, key: "description") == true){
            self.CoupondDescription = dict.valueForKey("description") as! String
        }
        if(self.valueForKeyExist(dict, key: "expires_at") == true){
            
            self.expires_at = dict.valueForKey("expires_at") as! String
        }
        if(self.valueForKeyExist(dict, key: "code") == true){
            
            self.code = dict.valueForKey("code") as! String
        }
        if(self.valueForKeyExist(dict, key: "header") == true){
            
            self.header = dict.valueForKey("header") as! String
        }
        
        if(self.valueForKeyExist(dict, key: "ad_id") == true){
            self.ad_id = (dict.valueForKey("ad_id") as! String)
        }
        
    }
}
